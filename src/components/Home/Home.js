import React, {Component} from 'react'
import './Home.css'
import {Link} from 'react-router-dom'

class Home extends Component {
  render () {
    return (
            <div className="Home">
                <div className="container">
              
                      <h2 >Hi, I Am </h2>
                      <h3>SAJJAD HOSSAIN</h3>
                    <p>Full Stack Developer</p>
                    <Link to='/contact'>Contact Me</Link> 
                </div>
            </div>
    )
  }
 }

 export default Home;