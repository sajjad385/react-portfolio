import React, {Component} from 'react'
import './Contact.css'

class Contact extends Component {
  render () {
    return (
            <div className="Contact">
                <div className="container">
                        <h3>Contact With ME</h3>
                        <p>Address : <span>Niketon Bazar, Road No-08, Dhaka-1212</span></p>
                        <p>Phone : <span>+88 018 8102 4385</span></p>
                        <p>Email : <span>cse.sajjad.hossain@gmail.com</span></p>
                </div>
            </div>
    )
  }
 }

 export default Contact;