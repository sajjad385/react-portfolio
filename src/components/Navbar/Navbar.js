import React, {Component} from 'react'

import {Navbar,Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'

class NavbarComponent extends Component {

  render(){
    return (
 <div className="Navbar">
       <div className="container-fuild">
       <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
           <Navbar.Brand><Link to='/'>MSHS</Link></Navbar.Brand>
           <Navbar.Toggle aria-controls="responsive-navbar-nav justify-content-end"/>
           <Navbar.Collapse id="responsive-navbar-nav" className='justify-content-end'>
               <Nav className='nav-color'>
                   <Nav.Link> 
                        <Link to='/'>Home</Link>
                   </Nav.Link>
                   <Nav.Link>
                        <Link to='/portfolio'>Portfolio</Link>
                   </Nav.Link>
                   <Nav.Link>
                        <Link to='/resume'>Resume</Link>
                   </Nav.Link>
                   <Nav.Link>
                   <Link to='/about'>About</Link>
                    </Nav.Link>
                   <Nav.Link >
                     <Link to='/contact'>Contact</Link>
                   </Nav.Link>

               </Nav>
           </Navbar.Collapse>
       </Navbar>
   </div> 
</div>         
    )
  }
}

export default NavbarComponent