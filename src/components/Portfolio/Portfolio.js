import React, {Component} from 'react'
import './Portfolio.css'
import {Card, Button,Col, Row, Container} from 'react-bootstrap'


const  url= "https://images.pexels.com/photos/20024/pexels-photo.jpg";
const  url1= "https://images.pexels.com/photos/20021/pexels-photo.jpg";
const  url2= "http://images.pexels.com/photos/23407/pexels-photo.jpg";
class Portfolio extends Component {
  render () {
    return (
            <div className="Portfolio">
                <Container>
                      <h3>Portfolio</h3>
                        <Row>
                        <Col md={4}  className='PortfolioCard'>
                           <Card>
                                <Card.Img variant="top" src={url} alt='image' height='200px' />
                                  <Card.Body>
                                    <Card.Title>Web Application</Card.Title>
                                    <Card.Text>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto commodi consequuntur dicta ea eius excepturi expedita harum ipsum
                                    </Card.Text>
                                    <Button variant="primary">Click To Show</Button>
                                  </Card.Body>
                            </Card>
                          </Col>
                          <Col md={4}  className='PortfolioCard'>
                           <Card>
                                <Card.Img variant="top" src={url1} alt='image' height='200px' />
                                  <Card.Body>
                                    <Card.Title>Graphics Design</Card.Title>
                                    <Card.Text>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto commodi consequuntur dicta ea eius excepturi expedita harum ipsum
                                    </Card.Text>
                                    <Button variant="primary">Click To Show</Button>
                                  </Card.Body>
                            </Card>
                          </Col>
                          <Col md={4}  className='PortfolioCard'>
                           <Card>
                                <Card.Img variant="top" src={url2} alt='image' height='200px' />
                                  <Card.Body>
                                    <Card.Title>Website Design</Card.Title>
                                    <Card.Text>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto commodi consequuntur dicta ea eius excepturi expedita harum ipsum
                                    </Card.Text>
                                    <Button variant="primary">Click To Show</Button>
                                  </Card.Body>
                            </Card>
                          </Col>
                          
                   </Row>
                  </Container>
            </div>
    )
  }
 }

 export default Portfolio;