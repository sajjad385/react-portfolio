import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'
import NavbarComponent from './components/Navbar/Navbar'
import Home from './components/Home/Home'
import About from './components/About/About'
import Contact from './components/Contact/Contact'
import Resume from './components/Resume/Resume'
import Portfolio from './components/Portfolio/Portfolio'
import Footer from './components/Footer/Footer'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <NavbarComponent/>
          <Switch>
            <Route path='/' exact>
                <Home/> 
            </Route>
            <Route path='/portfolio'>
              <Portfolio/> 
            </Route>
             <Route path='/resume'>
              <Resume/> 
            </Route>
             <Route path='/about'>
              <About/> 
            </Route>
            <Route path='/contact'>
              <Contact/> 
            </Route>
            </Switch>

            <Footer/>
        </Router>
      </div>
    );
  }
}

export default App;
